library ieee;
use ieee.std_logic_1164.all;

-- 1-Bit HalfAdder with Process --

entity halfadder is
	port(x,y:in bit;
	     s,c:out bit);
end halfadder;

architecture mimari of halfadder is
  begin
     process(x,y)
     begin
        if(x/=y) then
          s<='1';
        else
          s<='0'; 
        end if;
         if(x='1' and y='1') then
           c<='1';
         else 
           c<='0'; 
         end if;
    end process;
  end mimari;   
