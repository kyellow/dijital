library ieee;
use ieee.std_logic_1164.all;

 -- Decoder -- 

entity decoder is
port(enable,a0,a1,a2:in bit;
     d0,d1,d2,d3,d4,d5,d6,d7:out bit);
end decoder;

architecture mimari of decoder is
 begin
  process(enable,a0,a1,a2)
   begin
    if(enable='1') then
      if(a0='0' and a1='0' and a2='0') then
        d0<='1';d1<='0';d2<='0';d3<='0';d4<='0';d5<='0';d6<='0';d7<='0';end if;                                        
      if(a0='0' and a1='0' and a2='1') then
        d0<='0';d1<='1';d2<='0';d3<='0';d4<='0';d5<='0';d6<='0';d7<='0';end if; 
      if(a0='0' and a1='1' and a2='0') then
        d0<='0';d1<='0';d2<='1';d3<='0';d4<='0';d5<='0';d6<='0';d7<='0';end if; 
      if(a0='0' and a1='1' and a2='1') then
        d0<='0';d1<='0';d2<='0';d3<='1';d4<='0';d5<='0';d6<='0';d7<='0';end if; 
      if(a0='1' and a1='0' and a2='0') then
        d0<='0';d1<='0';d2<='0';d3<='0';d4<='1';d5<='0';d6<='0';d7<='0';end if; 
      if(a0='1' and a1='0' and a2='1') then
        d0<='0';d1<='0';d2<='0';d3<='0';d4<='0';d5<='1';d6<='0';d7<='0';end if; 
      if(a0='1' and a1='1' and a2='0') then
        d0<='0';d1<='0';d2<='0';d3<='0';d4<='0';d5<='0';d6<='1';d7<='0';end if;  
      if(a0='1' and a1='1' and a2='1') then
        d0<='0';d1<='0';d2<='0';d3<='0';d4<='0';d5<='0';d6<='0';d7<='1';end if;                                   
    end if;
      if(enable='0') then
        d0<='0';d1<='0';d2<='0';d3<='0';d4<='0';d5<='0';d6<='0';d7<='0';end if;                                                 
  end process;
end mimari;  
