library ieee;
use ieee.std_logic_1164.all;

-- Encoder -- 

entity encoder is
	port(d0,d1,d2,d3,d4,d5,d6,d7:in bit;
	     a0,a1,a2:out bit);
end encoder;

architecture mimari of encoder is
begin
process(d0,d1,d2,d3,d4,d5,d6,d7)
 begin
  if(d0='1'and d1='0'and d2='0'and d3='0'and d4='0'and d5='0'and d6='0'and d7='0')then
    a0<='0';a1<='0';a2<='0';end if;
  if(d0='0'and d1='1'and d2='0'and d3='0'and d4='0'and d5='0'and d6='0'and d7='0')then
    a0<='1';a1<='0';a2<='0';end if;
  if(d0='0'and d1='0'and d2='1'and d3='0'and d4='0'and d5='0'and d6='0'and d7='0')then
    a0<='0';a1<='1';a2<='0';end if;
  if(d0='0'and d1='0'and d2='0'and d3='1'and d4='0'and d5='0'and d6='0'and d7='0')then
    a0<='1';a1<='1';a2<='0';end if;
  if(d0='0'and d1='0'and d2='0'and d3='0'and d4='1'and d5='0'and d6='0'and d7='0')then
    a0<='0';a1<='0';a2<='1';end if;
  if(d0='0'and d1='0'and d2='0'and d3='0'and d4='0'and d5='1'and d6='0'and d7='0')then
    a0<='1';a1<='0';a2<='1';end if;
  if(d0='0'and d1='0'and d2='0'and d3='0'and d4='0'and d5='0'and d6='1'and d7='0')then
    a0<='0';a1<='1';a2<='1';end if;
  if(d0='0'and d1='0'and d2='0'and d3='0'and d4='0'and d5='0'and d6='0'and d7='1')then
    a0<='1';a1<='1';a2<='1';end if;  
end process;
end mimari;
                     
