library ieee;
use ieee.std_logic_1164.all;

entity devre is
port(x,y,z:in bit;
     s,c:out bit);
 end devre;

architecture mimari of devre is
	begin
	 process(x,y,z)
	  begin
	  if(x='0') then
	    if(y='0'and z='0') then
	     s<='0';c<='0';end if;
	    if(y/=z) then
	     s<='1';c<='0';end if;
	    if(y='1' and z='1') then
	     s<='0';c<='1';end if;
	  end if;
	  if(x='1') then
	    if(y=z) then
	     s<='0';c<='0';end if;
	    if(y='0' and z='1') then
	     s<='1';c<='1';end if; 
	    if(y='1' and z='0') then
	     s<='1';c<='0';end if;  
	  end if;
	end process;
end mimari;          
