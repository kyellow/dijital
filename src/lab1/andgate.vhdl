library ieee;
use ieee.std_logic_1164.all;

-- AND KAPISI --

entity andkapisi is

 port(X:in std_logic;		-- AND gate input
      Y:in std_logic;		-- AND gate input
      F:out std_logic);		-- AND gate output
      
end andkapisi;

architecture and_arc of andkapisi is

begin 
  
  F <=  X and Y;		-- 2 input AND gate
  
end and_arc;        
