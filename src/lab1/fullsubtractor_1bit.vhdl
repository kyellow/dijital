library ieee;
use ieee.std_logic_1164.all;

-- 1- Bit Full Subtractor --

entity fullsub_1_bit is

 port (X,Y,Cin:in BIT;
       S,Cout:out BIT);
       
 end fullsub_1_bit;
 
architecture full_arc of fullsub_1_bit is

  begin 
  
      S <= X xor Y xor Cin;
      
      Cout <= ((not X) and Y) or ((x xnor y) and Cin);       
      
 end full_arc;     
