library ieee;
use ieee.std_logic_1164.all;

-- 1-Bit Half Adder --

entity halfadder_1_bit is

  port (X,Y: in BIT;
        S,Cout:out BIT );

end halfadder_1_bit;

architecture half_arc of halfadder_1_bit is
 
 begin
 
    S <=  X xor Y;
    
    Cout <= X and Y;

end half_arc;
