library ieee;
use ieee.std_logic_1164.all;

-- NAND KAPISI --

entity nandkapisi is

 port(X:in std_logic;		-- NAND gate input 1
      Y:in std_logic;		-- NAND gate input 2
      F:out std_logic);		-- NAND gate output
      
end nandkapisi;

architecture nand_arc of nandkapisi is

begin 
  
  F <=  X nand Y;		-- 2 input NAND gate
  
end nand_arc;        
