library ieee;
use ieee.std_logic_1164.all;

-- 1-Bit Half Subtractor --

entity halfsub_1_bit is

  port (X,Y: in BIT;
        S,Cout:out BIT );

end halfsub_1_bit;

architecture half_arc of halfsub_1_bit is
 
 begin
 
    S <=  X xor Y;
    
    Cout <= (not X) and Y;

end half_arc;
