library ieee;
use ieee.std_logic_1164.all;

--XOR KAPISI--

entity xorkapisi is

  port (X:in std_logic;		 -- XOR gate input 1
        Y:in std_logic;		 -- XOR gate input 1
        F:out std_logic);	 -- XOR gate output
        
end xorkapisi;

architecture xor_arc of xorkapisi is

  begin
  
     F <= X xor Y;		-- 2 input exclusive-OR gate
     
  end xor_arc;
  
              
