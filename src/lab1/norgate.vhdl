library ieee;
use ieee.std_logic_1164.all;

--NOR Kapisi--

entity norkapisi is

  port(X:in std_logic;		-- NOR gate input 1 
       Y:in std_logic;		-- NOR gate input 2
       F:out std_logic);	-- NOR gate output

end norkapisi;
  
architecture nor_arc of norkapisi is   
 
   begin
  
        F <= X nor Y;		-- 2 input NOR gate
 
end nor_arc; 
