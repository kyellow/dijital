library ieee;

use ieee.std_logic_1164.all;

---Inverter(değil) Kapisi--

entity inverter is

 port (X:in std_logic;
       F:out std_logic);
       
 end inverter;
 
architecture inv_arc of inverter is

  begin 
       
      F <= not X ;
      
   end inv_arc;
       
