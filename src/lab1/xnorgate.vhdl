library ieee;
use ieee.std_logic_1164.all;

--XNOR KAPISI--

entity xnorkapisi is

  port (X:in std_logic;		 -- XNOR gate input 1
        Y:in std_logic;		 -- XNOR gate input 1
        F:out std_logic);	 -- XNOR gate output
        
end xnorkapisi;

architecture xnor_arc of xnorkapisi is

  begin
  
     F <= X xnor Y;		-- 2 input exclusive-NOR gate
     
  end xnor_arc;
  
              
