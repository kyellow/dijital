library ieee;
use ieee.std_logic_1164.all;

-- 1- Bit Full Adder --

entity fulladder_1_bit is

 port (X,Y,Cin:in BIT;
       S,Cout:out BIT);
       
 end fulladder_1_bit;
 
architecture full_arc of fulladder_1_bit is

  begin 
  
      S <= X xor Y xor Cin;
      
      Cout <= (X and Y) or (x and Cin) or (Cin and Y);       
      
 end full_arc;     
