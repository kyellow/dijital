library ieee;
use ieee.std_logic_1164.all;

--Deney Ad:Or Kapisi

entity orkapisi is

  port(X:in std_logic;		-- OR gate input
       Y:in std_logic;		-- OR gate input
       F:out std_logic);	-- OR gate output

end orkapisi;
  
architecture or_arc of orkapisi is   
 
   begin
  
        F <= X or Y;		-- 2 input OR gate
 
end or_arc; 
