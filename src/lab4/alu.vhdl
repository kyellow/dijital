library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;

entity alu is
port(a:in std_logic_vector(7 downto 0);
     b:in std_logic_vector(7 downto 0);
     sec:in std_logic_vector(3 downto 0);
     g:out std_logic_vector(7 downto 0);
     sifir:out std_logic;
     negatif:out std_logic);
end alu;

architecture mimarisi of alu is
 signal gecici:std_logic_vector(7 downto 0);
  begin 
   g<=gecici;
  process(sec,a,b)
   begin
    case sec is
     when "0000" => gecici<=a;
     when "0001" => gecici<=b;
     when "0010" => gecici<=(a+1);
     when "0011" => gecici<=(b+1);
     when "0100" => gecici<=(a-1);
     when "0101" => gecici<=(b-1);
     when "0110" => gecici<=(a+b);
     when "0111" => gecici<=(a-b);
     when "1000" => gecici<=a and b;
     when "1001" => gecici<=a or b;
     when "1010" => gecici<=a xor b;
     when "1011" => gecici<=not b;
     when "1100" => gecici<=(0-b);
     when "1101" => gecici<=not a;
     when "1110" => gecici<=(0-a);
     when "1111" => gecici<=a nand b;
     when others => gecici<=a;
    end case;
  end process;
  
isaret_kontrol:process(gecici)     
  begin
   if(gecici="00000000") then 
     sifir<='1';
   else 
     sifir<='0';
   end if;
   if(gecici(7)='1') then
     negatif<='1';
   else
     negatif<='0'; 
   end if;
 end process;
end mimarisi;
                 
            
