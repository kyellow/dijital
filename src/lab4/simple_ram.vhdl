Library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Sımple RAM --

Entity basitram is
Generic(k:integer:=10;
		n:integer:=8);
PORT(clk,oku,yaz : IN std_logic;
	 veri_girisi : IN std_logic_vector(n-1 downto 0);
	 veri_cikisi : OUT std_logic_vector(n-1 downto 0);
	 adres_secim : IN std_logic_vector(k-1 downto 0));
END basitram;

Architecture mimari1 of basitram is
type ram_tipim is array(0 to 2**k) of std_logic_vector(n-1 downto 0);
signal ram : ram_tipim;
Begin
	Process(clk)
	Begin
	if(clk'Event and clk='1') then
		if (yaz='1') then
	ram(conv_integer(adres_secim)) <= veri_girisi;
		ELSIF (oku='1') then
	veri_cikisi <= ram(conv_integer(adres_secim));
	End if;
End if;
End Process;
End mimari1;
