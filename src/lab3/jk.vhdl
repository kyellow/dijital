library ieee;
use ieee.std_logic_1164.all;

-- JK Flip-Flop --

entity jk is
 port(j,k,saat,reset:in std_logic;
      q,qd:out std_logic);
end jk;

architecture mimari of jk is     
  signal cikis:std_logic;
  signal giris:std_logic_vector(1 downto 0);
begin
 giris<=j&k;
process(saat,reset) 
  begin
    if(reset='1')then cikis<='0';
      else if(rising_edge(saat))then 
        case(giris) is   
         when "11" => cikis <= reset;
         when "10" => cikis<='1';
         when "01" => cikis<='0';
         when others => null;
        end case;
      end if; 
    end if;    
end process;
 q <= cikis;     
 qd<= not cikis; 
end mimari; 
