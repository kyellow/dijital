library ieee;
use  ieee.std_logic_1164.all;

-- 8-bit 4 to 1 Multiplexer --

entity mux is
 port(	A:in std_logic_vector(7 downto 0);		-- inputs
	B:in std_logic_vector(7 downto 0);		-- inputs
	C:in std_logic_vector(7 downto 0);		-- inputs
	D:in std_logic_vector(7 downto 0);		-- inputs      
	S:in std_logic_vector(1 downto 0);		-- select input
	O:out std_logic_vector(7 downto 0));		-- output
end mux;

architecture mimari of mux is
 begin
  process(A, B, C, D, S)
   begin
    case S is
      when "00" => O <= A;
      when "01" => O <= B;
      when "10" => O <= C;
      when "11" => O <= D;
      when others=> O <= "00000000";
     end case;
  end process;
end mimari;     
