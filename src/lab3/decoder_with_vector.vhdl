library ieee;
use  ieee.std_logic_1164.all;

entity decoder is
 port(I:in std_logic_vector(1 downto 0);
      E:in std_logic;
      O:out std_logic_vector(3 downto 0));
end decoder;

architecture mimari of decoder is
 begin
  process(I,E)
   begin
    if(E='1') then
     case(I) is
      when "00" =>  O<="0001";
      when "01" =>  O<="0010";     
      when "10" =>  O<="0100";
      when "11" =>  O<="1000";
     end case;
    else O<="0000";
    end if;
  end process;
end mimari;     
