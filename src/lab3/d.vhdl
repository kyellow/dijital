Library ieee;
use ieee.std_logic_1164.all;

-- D Flip-FLop --

Entity d_ff IS
	Port(d,rst,clk : IN BIT;
		 q : OUT BIT);
End d_ff;

Architecture mimari1 of d_ff IS
	SIGNAL temp:BIT;
Begin
	Process(clk, rst)
	Begin
		IF ( rst = '1' ) then temp <='0';
		ELSE IF (clk 'EVENT and clk='1') then temp <= d;
		END IF;
		q <= temp;
	End Process;
End mimari1;
