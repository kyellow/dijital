# dijital
Assignments for Digital Systems Design (Spring 2012)

## Description
VHDL components and entities developed for the digital systems design course

 * Lab 1
   * AND Gates
   * NAND Gates
   * OR Gates
   * NOR Gates
   * XOR Gates
   * XNOR Gates
   * Inverter Gates
   * Half Adder (1 Bit)
   * Half Subtractor (1 Bit)
   * Full Adder (1 Bit)
   * Full Subtractor (1 Bit)
 * Lab 2
   * Decoder
   * Encoder
   * Combinational Circuits
   * Half Adder (With Process)
 * Lab 3
   * JK Flip-Flop
   * D Flip-Flop
   * Decoder (With Logic Vector)
   * Multiplexer
 * Lab 4
   * ALU
   * Simple RAM 
